<div align="center">

<img src="https://ih1.redbubble.net/image.522185564.8141/st,small,507x507-pad,600x600,f8f8f8.u5.jpg" alt="logo" width="120" />

</div>

<br/>

---

- [awsprofile](#awsprofile)
- [install_kubie](#install_kubie)
- [update_awscli](#update_awscli)
- [vpn-fix](#vpn-fix)

---
<br/>
<br/>

# awsprofile

![img](https://img.shields.io/badge/bash-black.svg?style=plastic&logo=gnubash)
![img](https://img.shields.io/badge/Linux-blue.svg?style=plastic&logo=linux)
![img](https://img.shields.io/badge/macOS-gray.svg?style=plastic&logo=apple)

Selector de perfil de usuario IAM de AWS.

Permite seleccionar visualmente el perfil de AWS  para luego anclar una subshell en el perfil que hayas seleccionado. Para salir del perfil presiona `Crtl`+`d`.

Utiliza `fzf` (*fuzzy-finder*), un buscador de línea de comandos de propósito general.

> Requisitos:
> 1. `curl`
> 1. `jq`

[![](https://img.shields.io/badge/fzf-black.svg?style=plastic&logo=github)](https://github.com/junegunn/fzf)

<br/>
<br/>

---
# install_kubie

![img](https://img.shields.io/badge/bash-black.svg?style=plastic&logo=gnubash)
![img](https://img.shields.io/badge/Linux-blue.svg?style=plastic&logo=linux)
![img](https://img.shields.io/badge/macOS-gray.svg?style=plastic&logo=apple)


Instala `kubie`, una alternativa a (**kubectx** + **kubens** + **kubeselect**).

Adicionalmente instala `fzf` (*fuzzy-finder*), un buscador de línea de comandos de propósito general. Te permitirá navegar visualmente los contextos (*al ejecutar `kubie ctx`*) o namespaces (*al ejecutar `kubie ns`*) para luego anclar una subshell en el contexto y/o namespace que hayas seleccionado. Sales con `Crtl`+`d`.

Al ejecutarse por primera vez, creará el archivo de configuración `~/.kube/kubie.yaml` y la carpeta `~/.kube/configs/` donde podrás guardar tus contextos en formato YAML.

Visita la [guia de uso](https://github.com/sbstp/kubie#usage) para aprovecharlo al máximo!

> Requisitos:
> 1. `sudo`
> 1. `curl`
> 1. `jq`

[![](https://img.shields.io/badge/kubie-black.svg?style=plastic&logo=github)](https://github.com/sbstp/kubie) [![](https://img.shields.io/badge/fzf-black.svg?style=plastic&logo=github)](https://github.com/junegunn/fzf)

<br/>
<br/>

---

# update_awscli

![img](https://img.shields.io/badge/bash-black.svg?style=plastic&logo=gnubash)
![img](https://img.shields.io/badge/Linux-blue.svg?style=plastic&logo=linux)

Instala la herramienta de línea de comandos `aws`.
Si ésta ya se encuentra instalada, `update_awscli` buscará actualizaciones e instalará la última la versión disponible.

> Requisitos:
> 1. `sudo`
> 1. `curl` (*o `wget`*)
> 1. `unzip`

```
Usage:
      update_awscli [arg]
Args:
      (none)   instala/actualiza awscli v2
      -r       desinstala awscli v2
```

[![](https://img.shields.io/badge/awscli--v2-orange.svg?style=plastic&logo=amazon-aws)](https://docs.aws.amazon.com/cli/latest/userguide/)

<br/>
<br/>

---

# vpn-fix

![img](https://img.shields.io/badge/bash-black.svg?style=plastic&logo=gnubash)
![img](https://img.shields.io/badge/Linux-blue.svg?style=plastic&logo=linux)

Script para corregir resolución DNS y rutas de red (*split-tunnel*) luego de establecer una conexión VPN.

Esto es útil cuando tu trabajo se basa 100% en una conexión VPN corporativa, y por cuestiones de seguridad/performance necesitas dividir el tráfico para que sólo lo estrictamente necesario utilice dicha conexión.

Detecta API servers de kubernetes en tu `~/.kube/config`, como así también los API servers en archivos de configuración de [kubie](#install_kubie) creando rutas para c/u de ellos a través de tu interfaz VPN.

> Requisitos:
> 1. `sudo`
> 1. `host`
> 1. `awk`
>
> Debes editar en el script:
> * `VPN_CIDRS` (*línea 18*)
> * `NAMESERVERS` (*línea 22*)
>
> Ejemplo, si tu red corporativa utiliza los bloques `192.168.40.0/21` y `172.16.0.0/16` deberás configurar:
> * `VPN_CIDRS="192.168.40.0/21 172.16.0.0/16"`
> * `NAMESERVERS="8.8.8.8 <dns1-para-vpn> <dns2-para-vpn>"`
>
> De este modo, se crearán rutas para que todo el tráfico con destino `192.168.40.0/21` y `172.16.0.0/16` será enviado a través de la interfaz del tunnel VPN, mientras que el resto del tráfico (*ej: internet*) irá directo por tu conexión local sin pasar por la red corporativa.
>
> Adicionalmente, la resolución de nombres utilizará *-en este ejemplo-* primero el NS de Google `8.8.8.8` y sólo utilizará los NSs `<dns1-para-vpn>` y `<dns2-para-vpn>` cuando el anterior no logre resolver el request.

```
Usage:
      sudo vpn-fix
```
